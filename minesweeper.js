$(document).ready(startGame());

/**********************
/* GAME START
/**********************/
function startGame(){
	$(document.body).empty();
	//console.log("Starting Game");
	$(document.body).append("<h1> Minesweeper");
	var buttonsDifficulty = ["Anfänger", "Fortgeschrittene", "Profis"];
	var wrapButtons = $("<div/>", {class: "difficulty"});

	//add Listener to Buttons and add them to the div region
	buttonsDifficulty.forEach(function (buttons){
		//console.log(buttons);
		var button = $("<button/>", {
		class: "difficulty",
		text: buttons});
		switch (buttons){
			case "Anfänger": var nrBombs = 10;
					break;
			case "Fortgeschrittene": var nrBombs = 15;
					break;
			case "Profis": var nrBombs = 20;
					break;
		}
		button.on("click", function (){
			createBoard(nrBombs);
		});
		button.appendTo(wrapButtons);
		wrapButtons.append("<br>");
	});

	$(document.body).append(wrapButtons);
}

function createBoard(nrBombs){
	$("p").remove();
	$(".difficulty").remove();
	var size = 10;
	playingBoard = new Board(size, nrBombs);
	playingBoard.initialize(nrBombs);
	drawBoard(playingBoard);
}

function drawBoard(playingBoard){
	//draw remaining Bombs and Timer
	var remBombs = $('<div>', {'id': 'bombs'});
	remBombs.text(playingBoard.bombs);
	var timerDisplay = $('<div>', {'id': 'time'});
	timerDisplay.text("000");
	var divTemp = $('<div>', {'class': 'bombs-timer'});
	divTemp.append(remBombs);
	divTemp.append(timerDisplay);
	divTemp.appendTo($(document.body));

	//draw Board
	var table = $('<table/>', {"class": "board"});
	for(var i = 0; i < playingBoard.y; i++){
		var row = $('<tr/>', {"class": "board"});
		for(var j = 0; j < playingBoard.x; j++){
			var button = $('<button/>', {"data-x": j, "data-y": i, "class": "board", "data-clicked": "false"});
			button.text("");
			//disable contextmenu for right click on buttons
			button.contextmenu(function(){
				return false;
			});
			var td = $('<td/>', {"class": "board"});
			button.mousedown(function (event){
				var idx = event.target.getAttribute("data-x");
				var idy = event.target.getAttribute("data-y");
				switch(event.which){
					case 1: playingBoard.openButton(idx, idy);
							break;
					case 3: flagButton(idx, idy, $('#bombs'));
							break;
				}
				//check if all fields are open then go to Game Won!
				if(playingBoard.allOpen()){
					gameWon(playingBoard);
				}
			});
			button.appendTo(td);
			td.appendTo(row);
		}
		row.appendTo(table);
	}
	table.appendTo($(document.body));
}

/**********************
/* BOARD
/**********************/
function Board(size, nrBombs){
	this.x = size;
	this.y = size;
	this.firstButton = true;
	this.bombs = nrBombs;
	this.board = new Array(size);
	this.timer = new Timer();
	for(var i = 0; i < this.y; i++){
		this.board[i] = new Array(size);
	}
}

Board.prototype.initialize = function(nrBombs){
	//console.log("Bin in initialize");
	var mines = [];

	//randomly generate positions for the mines
	while(mines.length < nrBombs){
		//console.log(mines.length);
		var mine = new Vector(Math.floor(Math.random()*this.x), Math.floor(Math.random()*this.y));
		if(!containsVector(mines, mine)){
			mines.push(mine);
			//console.log("x " + mine.x + " y " + mine.y);
		}
	}

	//initialize board with 0 neighbouring mines
	for(var i = 0; i < this.y; i++){
		for(var j = 0; j < this.x; j++){
			//0 neighbouring mines
			this.board[j][i] = 0;
		}
	}

	//add mines to board
	for(var k = 0; k < mines.length; k++){
		var mine = mines[k];
		//-1 is a mine!
		this.board[mine.x][mine.y] = -1;
	}

	//calculate for every position the number of neighbouring mines
	//loop through the Array
	for(var i = 0; i < this.y; i++){
		for(var j = 0; j < this.x; j++){
			//check if current field is not a mine
			if(this.board[j] && this.board[j][i] !== -1){
				//loop through neighbours (8-connected)
				for(var k = i-1; k <= i+1; k++){
					for(var l = j-1; l <= j+1; l++){
						//if neighbour is a mine, increment field by one
						if(this.board[l] && this.board[l][k] === -1){
							this.board [j][i] += 1;
						}
					}
				}
			}
		}
	}
};

Board.prototype.openButton = function(x, y){
	if(this.firstButton){
		this.timer.startTimer($('#time'));
		this.firstButton = false;
	}

	var number = this.board[x][y];
	var button = $("button[data-x = "+x+"][data-y = "+y+"]");

	if(button.attr("data-clicked") == 'flag'){
		var bombsDisplay = $('#bombs');
		var bombsText = bombsDisplay.text();
		bombsDisplay.text(+bombsText +1);
	}
	button.off();
	button.attr("data-clicked", "true");

	if(number > 0){
		button.text(number);
		switch (number){
			case 1: button.css("color", "blue");
			break;
			case 2: button.css("color", "green");
			break;
			case 3: button.css("color", "red");
			break;
			case 4: button.css("color", "#000080");
			break;
			case 5: button.css("color", "#8A0000");
			break;
		}
		this.board[x][y] = -2; //-2 means the button was opened
	}
	else if(number === -1){
		//document.body).append("<p/> You Lost!");
		gameOver(button, this);
	}else if(number === 0){
		this.board[x][y] = -2; //the button was opened and this will stop recursion
		//loop through neighbours (8-connected)
		//console.log("vorher: x: "+x+" ,y: "+y);
		var loweri = +y-1; var upperi = +y+1;
		var lowerj = +x-1; var upperj = +x+1;
		for(var i = loweri; i <= upperi; i++){
			for(var j = lowerj; j <= upperj; j++){
				//check if neighbour already open ==> signal to stop recursion; else open neighbour
				if(this.board[j] && !(this.board[j][i] === -2)){
					this.openButton(j, i);
				}
			}
		}
	}
};

Board.prototype.allOpen = function (){
	for(var i = 0; i < this.board[0].length; i++){
		for(var j = 0; j < this.board.length; j++){
			if(this.board[j][i] >= 0){
				return false;
			}
		}
	}
	return true;
};

/**********************
/* GAMEPLAY
/**********************/
function flagButton(x, y, display){
	var button = getButton(x, y);
	var remBombs = display.text();

	if(button.attr("data-clicked") === "false"){
		button.attr("data-clicked", "flag");
		remBombs = +remBombs - 1;
		display.text(remBombs);
	}else if(button.attr("data-clicked") === "flag"){
		button.attr("data-clicked", "false");
		remBombs = +remBombs + 1;
		display.text(remBombs);
	}
}

function getButton(x, y){
	return $("button[data-x = "+x+"][data-y = "+y+"]");
}

/**********************
/* GAME END
/**********************/
function gameOver(button, playingBoard){
	//stop Timer
	playingBoard.timer.stopTimer();

	//explode bomb
	var bomb = $("<div/>", {id: "bombAnimation"});
	bomb.appendTo(button);
	bomb.animate({backgroundColor: "red"}, 1000);

	//loop through board to remove all Listeners and return all bombs
	//var x = Number(button.attr("data-x"));
	//var y = Number(button.attr("data-y"));
	var allBombs = [];
	for(var i = 0; i < playingBoard.y; i++){
		for(var j = 0; j < playingBoard.x; j++){
			var newButton = function(j, i){
				return getButton(j, i)
			}(j, i);
			//remove Listener
			newButton.off();
			//remove Flags
			newButton.attr("data-clicked", "false");
			//add Bombs to allBombs array
			if(playingBoard.board[j][i] === -1){
				allBombs.push(newButton);
			}
		}
	}

	setTimeout(function(){
		//open all other bombs
		for(var k = 0; k < allBombs.length; k++){
			//console.log(newButton);
			var newBomb = $("<div/>", {id: "bombAnimation"});
			newBomb.appendTo(allBombs[k]);
			newBomb.animate({backgroundColor: "red"}, 1000);
		}
	}, 1000);

	var timeout = setTimeout(function(){
		clearTimeout(timeout);
		endGame("Du bist schlecht, übe noch ein wenig!", "lost", playingBoard);
	}, 4000);
}

function gameWon(playingBoard){
	playingBoard.timer.stopTimer();
	endGame("You Won! Congratulations.", "won", playingBoard);
}

function endGame(text, id, playingBoard){
	$("table.board").fadeOut(2000, function(){
		var endingText = $("<div/>",
		{id: id,
		text: text});
		$(document.body).append(endingText);

		if(id === "won"){
			setTimeout(function(){
				$("#"+id).fadeOut("slow", writeHighscore(playingBoard));
			}, 3000);
		}else{
			setTimeout(function(){
				$("#"+id).fadeOut("slow", startGame());
			}, 3000);
		}

	});
}

/**********************
/* HIGHSCORE
/**********************/
function writeHighscore(playingBoard){
	var bombs = playingBoard.bombs;
	var localScore;

	//Retrieve from local Storage
	switch (bombs){
		case 10: localScore = JSON.parse(localStorage.getItem("localScore_Anf"));
		break;
		case 15: localScore = JSON.parse(localStorage.getItem("localScore_Fort"));
		break;
		case 20: localScore = JSON.parse(localStorage.getItem("localScore_Pro"));
		break;
	}

	//Ask for Name
	var time = $('#time').text();
	var name = prompt("Please enter your name");

	//Save to local storage
	var newEntry = {
		'time': time,
		'name': name};
	if(localScore === null){
		localScore = {};
		localScore.entries = [];
	}
	localScore.entries.push(newEntry);
	sortHighscore(localScore);

	switch (bombs){
		case 10: localStorage.localScore_Anf = JSON.stringify(localScore);
		break;
		case 15: localStorage.localScore_Fort = JSON.stringify(localScore);
		break;
		case 20: localStorage.localScore_Pro = JSON.stringify(localScore);
		break;
	}

	showHighscore(localScore);
}

function sortHighscore(localScore){
	var array = localScore.entries;
	console.log(Number(array[0].time));

	array.sort(function(a, b) {
		return parseFloat(Number(a.time)) - parseFloat(Number(b.time));
	});

	if(array.length > 10){
		console.log("gepoppt");
		array.pop();
	}
}

function showHighscore(localScore){
	var array = localScore.entries;

	var table = $('<table/>', {"class": "highscore"});
	var row = $('<tr/>', {"class": "highscore"});
	row.append($('<th> Name </th>'));
	row.append($('<th> Zeit </th>'));
	table.append(row);
	array.forEach(function(entry){
		var row = $('<tr/>', {"class": "highscore"});
		var td_1 = $('<td/>', {"class": "name"});
		td_1.html(entry.name);
		var td_2 = $('<td/>', {"class": "time"});
		td_2.html(entry.time);
		td_1.appendTo(row);
		td_2.appendTo(row);
		row.appendTo(table);
	});
	$(document.body).empty();
	$(document.body).append(table);

	var newGame = $('<button/>', {
				class: "newGame",
				text: "new Game"});
	newGame.mousedown(function(event){
		startGame();
	});
	$(document.body).append($('<div/>').append(newGame));
}


/**********************
/* TIMER
/**********************/
function Timer(){
	var timerID = 0;
}

Timer.prototype.startTimer = function (display){
	//console.log(display);
	var start = Date.now();
	var seconds;

	function timer(){
		seconds = (((Date.now() - start) / 1000) | 0);

		if(seconds < 100){
			if(seconds < 10){
				seconds = "0" + seconds;
			}
			seconds = "0" + seconds;
		}
		//console.log(seconds);
		display.text(seconds);
	};
	timer();
	this.timerID = setInterval(timer, 1000);
};

Timer.prototype.stopTimer = function (){
	clearInterval(this.timerID);
};

/**********************
/* DATATYPES
/**********************/
function Vector(x, y){
	this.x = x; this.y = y;
}

Vector.prototype.equals = function (other){
	if(! other instanceof Vector){
		return false;
	}
	if(this.x === other.x && this.y === other.y){
		return true;
	}
	return false;
};

function containsVector(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i].equals(obj)) {
            return true;
        }
    }
    return false;
}
